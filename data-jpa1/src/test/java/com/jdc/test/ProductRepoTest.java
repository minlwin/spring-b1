package com.jdc.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jdc.spring.data.entity.Product;
import com.jdc.spring.data.repo.ProductRepo;

class ProductRepoTest {
	
	static ConfigurableApplicationContext ctx;
	ProductRepo repo;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new ClassPathXmlApplicationContext("META-INF/application.xml");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}
	
	@BeforeEach
	void start() {
		repo = ctx.getBean(ProductRepo.class);
	}

	@Test
	void test1() {
		
		Product p = new Product();
		p.setName("Coke");
		p.setPrice(300);
		
		repo.save(p);
		
		assertEquals(1, p.getId());
	}
	
	@Test
	void test2() {
		
		List<Product> list1 = repo.findByPrices(300, 301);
		assertEquals(1, list1.size());
		
		
		List<Product> list2 = repo.findByPrices(299, 300);
		assertEquals(1, list2.size());

		List<Product> list3 = repo.findByPrices(228, 229);
		assertEquals(0, list3.size());
}

}
