package com.jdc.spring.data.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jdc.spring.data.entity.Product;

public interface ProductRepo extends JpaRepository<Product, Integer>{

	List<Product> findByNameLike(String name);
	
	@Query
	List<Product> findByPrices(@Param("from") int from, @Param("to") int to);
	
	List<Product> findByNameStartingWithIgnoreCase(String name);
}
