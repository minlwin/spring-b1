package com.jdc.spring.mvc.controller.common;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy= StringOnlyValidator.class)
@Retention(RUNTIME)
@Target(FIELD)
public @interface StringOnly {

	String message() default "You should enter Char Only!";
	Class<?> [] groups() default {};
	Class<? extends Payload> [] payload() default {};
	
	int length() default 0;
	boolean upperOnly() default false;
}
