package com.jdc.spring.mvc.controller.common;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StringOnlyValidator implements ConstraintValidator<StringOnly, String> {
	
	private StringOnly annotaion;
	
	@Override
	public void initialize(StringOnly constraintAnnotation) {
		annotaion = constraintAnnotation;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		
		if(null != value) {
			if(annotaion.upperOnly()) {
				if(!value.equals(value.toUpperCase())) {
					return false;
				}
			}
			
			if(annotaion.length() > 0) {
				if(value.length() > annotaion.length()) {
					return false;
				}
			}
		}
		
		return true;
	}

}
