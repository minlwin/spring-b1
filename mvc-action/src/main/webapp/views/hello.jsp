<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Hello Spring MVC</title>
</head>
<body>

	<h1>Hello Spring MVC</h1>

	<c:url var="category" value="/categories" />
	<c:url var="item" value="/items" />
	
	<p>
		<c:out value="${message}"></c:out>
	</p>
	
	<ul>
		<li>
			<a href="${category}">Category</a>
		</li>
		<li>
			<a href="${item}">Items</a>
		</li>
	</ul>

</body>
</html>