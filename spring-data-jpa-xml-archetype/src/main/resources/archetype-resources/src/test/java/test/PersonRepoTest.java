#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ${package}.entity.Person;
import ${package}.repo.PersonRepo;

class PersonRepoTest {
	
	static ConfigurableApplicationContext ctx;
	PersonRepo repo;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new ClassPathXmlApplicationContext("application.xml");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}

	@BeforeEach
	void setUp() throws Exception {
		repo = ctx.getBean(PersonRepo.class);
	}

	@Test
	@DisplayName("Insert Test")
	void test1() {
		Person p = new Person();
		p.setName("Aung Aung");
		repo.save(p);
		
		assertEquals(1, p.getId());
	}
	
	@Test
	@DisplayName("Find By Id Test")
	void test2() {
		
		Optional<Person> optional = repo.findById(1);
		Person p = optional.get();
		
		assertNotNull(p);
		assertEquals("Aung Aung", p.getName());
	}

}
