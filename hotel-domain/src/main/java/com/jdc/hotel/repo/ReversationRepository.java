package com.jdc.hotel.repo;

import org.springframework.stereotype.Repository;

import com.jdc.hotel.entity.Reservation;

@Repository
public class ReversationRepository extends BaseRepository<Reservation> {

	public ReversationRepository() {
		super(Reservation.class);
	}

}
