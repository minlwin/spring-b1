package com.jdc.emdemo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.jdc.emdemo.listener.SecureEntity;

@Entity
public class Classes implements SecureEntity {

	private static final long serialVersionUID = 1L;

	@Id
	private ClassesPK pk;
	@ManyToOne
	@JoinColumn(insertable = false, updatable = false)
	private Branch branch;
	@ManyToOne
	@JoinColumn(insertable = false, updatable = false)
	private Course course;
	private double duration;
	private SecurityInfo security;

	public Classes() {
		pk = new ClassesPK();
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public ClassesPK getPk() {
		return pk;
	}

	public void setPk(ClassesPK pk) {
		this.pk = pk;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
		pk.setBranchId(branch.getId());
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
		pk.setCourseId(course.getId());
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

}
