package com.jdc.spring.jpa.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class PersonRepo {

	@PersistenceContext
	private EntityManager em;

	public EntityManager getEm() {
		return em;
	}
}
