package com.jdc.spring.mvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jdc.spring.mvc.entity.Category;
import com.jdc.spring.mvc.service.CategoryService;

@Controller
@RequestMapping("categories")
public class CategoryController {
	
	@Autowired
	private CategoryService service;
	
	@GetMapping
	public String getAll(ModelMap model) {
		List<Category> list = service.getAll();
		model.put("list", list);
		return "category-list";
	}
	
	@GetMapping("/edit")
	public String addNew() {
		
		return "category-edit";
	}
	
	@GetMapping("/edit/{id:\\d+}")
	public String edit(@PathVariable int id, ModelMap model) {
		service.findById(id).ifPresent(c -> {
			model.put("category", c);
		});
		return "category-edit";
	}
	
	@PostMapping
	public String save(@ModelAttribute @Valid Category category, BindingResult result) {
		
		if(result.hasErrors()) {
			return "/category-edit" + (category.getId() > 0 ? "/" + category.getId() : "");
		}
		
		service.save(category);
		
		return "redirect:/categories";
	}
	
	@PostMapping("/delete/{id:\\d+}")
	public String delete(@PathVariable int id) {
		service.delete(id);
		return "redirect:/categories";
	}
	
	@ModelAttribute("category")
	public Category category() {
		return new Category();
	}

}
