package com.jdc.tinypos.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class SaleItem extends StockAction implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	private Voucher voucher;

	public Voucher getVoucher() {
		return voucher;
	}

	public void setVoucher(Voucher voucher) {
		this.voucher = voucher;
	}	

}