package com.jdc.spring.mvc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdc.spring.mvc.entity.Category;
import com.jdc.spring.mvc.repo.CategoryRepo;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepo repo;

	public List<Category> getAll() {
		return repo.findAll();
	}

	public Optional<Category> findById(int id) {
		return repo.findById(id);
	}

	@Transactional
	public void save(Category category) {
		repo.save(category);
	}

	@Transactional
	public void delete(int id) {

		repo.findById(id).ifPresent(c -> {
			c.setDeleted(true);
			repo.save(c);
		});
	}
}
