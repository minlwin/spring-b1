package com.jdc.tinypos.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class PurchaseItem extends StockAction implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Invoice invoice;

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
	
	
}