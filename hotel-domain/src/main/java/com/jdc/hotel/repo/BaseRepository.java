package com.jdc.hotel.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

public abstract class BaseRepository<T extends JpaEntity> {
	
	protected Class<T> type;

	@PersistenceContext
	protected EntityManager em;
	
	
	public BaseRepository(Class<T> type) {
		super();
		this.type = type;
	}

	@Transactional
	public void create(T t) {
		em.persist(t);
	}
	
	public T findById(Object id) {
		return em.find(type, id);
	}
	
	@Transactional
	public void update(T t) {
		em.merge(t);
	}
	
	@Transactional
	public void delete(T t) {
		em.remove(em.getReference(type, t.getId()));
	}
}
