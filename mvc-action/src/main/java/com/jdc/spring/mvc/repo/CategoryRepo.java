package com.jdc.spring.mvc.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jdc.spring.mvc.entity.Category;

public interface CategoryRepo extends JpaRepository<Category, Integer> {

}
