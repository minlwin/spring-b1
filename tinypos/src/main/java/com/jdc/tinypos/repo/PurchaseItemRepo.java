package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.PurchaseItem;

@Repository
public class PurchaseItemRepo extends AbstractRepository<PurchaseItem> {

	public PurchaseItemRepo() {
		super(PurchaseItem.class);
	}
}
