insert into Course (name, price, delFlag) values ('Java SE', 180000, false);
insert into Course (name, price, delFlag) values ('Java EE', 300000, false);
insert into Course (name, price, delFlag) values ('Kotlin Basic', 180000, false);
insert into Course (name, price, delFlag) values ('Android Basic', 180000, false);
insert into Course (name, price, delFlag) values ('Android Advance', 200000, false);
insert into Course (name, price, delFlag) values ('Spring', 300000, false);
