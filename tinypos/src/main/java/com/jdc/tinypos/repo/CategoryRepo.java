package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.Category;

@Repository
public class CategoryRepo extends AbstractRepository<Category> {

	public CategoryRepo() {
		super(Category.class);
	}
}
