package com.jdc.spring.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jdc.spring.data.entity.Person;

public interface PersonRepo extends JpaRepository<Person, Integer>{

}
