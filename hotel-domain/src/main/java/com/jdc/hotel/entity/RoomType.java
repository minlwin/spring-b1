package com.jdc.hotel.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;

import com.jdc.hotel.repo.JpaEntity;
import static javax.persistence.FetchType.EAGER;

@Entity
public class RoomType implements JpaEntity {

	private static final long serialVersionUID = 1L;

	public RoomType() {
		facilities = new LinkedHashMap<>();
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;

	private String type;

	@ElementCollection(fetch = EAGER)
	@CollectionTable(name="Facilities")
	@MapKeyColumn(name = "type")
	private Map<String, String> facilities;

	private int price;

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, String> getFacilities() {
		return facilities;
	}

	public void setFacilities(Map<String, String> facilities) {
		this.facilities = facilities;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public void addFacility(String name, String value) {
		facilities.put(name, value);
	}
	
	public void removeFacility(String name) {
		facilities.remove(name);
	}

}