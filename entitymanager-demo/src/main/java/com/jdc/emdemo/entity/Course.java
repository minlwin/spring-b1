package com.jdc.emdemo.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.jdc.emdemo.listener.SecureEntity;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "Course.findAll", query = "select c from Course c where c.security.delFlag = false"),
	@NamedQuery(name = "Course.deleteById", query = "delete from Course c where c.id = :id"),
	@NamedQuery(name = "Course.findByNameLike", query = "select c from Course c where c.name like :name and c.security.delFlag = false") 
})
public class Course implements SecureEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	private String name;
	private int price;
	private SecurityInfo security;
	
	@ElementCollection
	@MapKeyColumn(name="studentId")
	private Map<String, Integer> ratings;
	
	public Course() {
		ratings = new LinkedHashMap<>();
	}
	
	public Integer getRate() {
		return ratings.values().stream().mapToInt(i -> i).sum() / ratings.size();
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Map<String, Integer> getRatings() {
		return ratings;
	}

	public void setRatings(Map<String, Integer> ratings) {
		this.ratings = ratings;
	}

}
