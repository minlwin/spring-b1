package com.jdc.emdemo.entity;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.jdc.emdemo.listener.SecureEntity;

@Entity
public class Branch implements SecureEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	private String name;
	private String address;

	@OneToMany(mappedBy = "branch", cascade = { PERSIST, MERGE }, orphanRemoval = true)
	private List<ClassRoom> rooms;

	private SecurityInfo security;

	public Branch() {
		rooms = new ArrayList<>();
	}

	public void addRoom(ClassRoom room) {
		room.setBranch(this);
		rooms.add(room);
	}

	public void remove(ClassRoom room) {
		room.setBranch(null);

		Iterator<ClassRoom> itr = rooms.iterator();

		while (itr.hasNext()) {
			ClassRoom current = itr.next();
			if (current.getId() == room.getId()) {
				itr.remove();
			}
		}
	}

	public List<ClassRoom> getRooms() {
		return rooms;
	}

	public void setRooms(List<ClassRoom> rooms) {
		this.rooms = rooms;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

}
