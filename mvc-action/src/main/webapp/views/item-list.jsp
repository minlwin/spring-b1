<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Item List</title>
</head>
<body>

	<h1>Item List</h1>
	
	<div>
		<c:url value="/items/add" var="add" />
		<a href="${add}">Add New Item</a>
	</div>
	
	<table>
		<thead>
			<tr>
				<th>ID</th>
				<th>Category</th>
				<th>Name</th>
				<th>Price</th>
				<th></th>
			</tr>
		</thead>
		
		<tbody>
			<c:forEach items="${list}" var="item">
				<tr>
					<td>${item.id}</td>
					<td>${item.category.name}</td>
					<td>${item.name}</td>
					<td>${item.price}</td>
					<td>
						<c:url var="edit" value="/items/edit">
							<c:param name="id" value="${item.id}" />
						</c:url>
						<a href="${edit}">Edit</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
</body>
</html>