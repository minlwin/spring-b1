package com.jdc.tinypos.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jdc.tinypos.entity.Category;
import com.jdc.tinypos.entity.Product;
import com.jdc.tinypos.repo.ProductRepo;

@Service
public class ProductService {

	@Autowired
	private ProductRepo repo;
	
	public List<Product> findByCategory(Category c) {
		
		Map<String, Object> params = new HashMap<>();
		params.put("category", c);
		
		return repo.selectNamedQuery("Product.findByCategory", params);
	}
	
	public List<Product> findByDateAndPrice(LocalDate date, int price) {
		Map<String, Object> params = new HashMap<>();
		params.put("date", date);
		params.put("price", price);
		
		return repo.selectNamedQuery("Product.findByDateAndPrice", params);
	}
}
