package com.jdc.hotel.repo;

import org.springframework.stereotype.Repository;

import com.jdc.hotel.entity.Floor;

@Repository
public class FloorRepository extends BaseRepository<Floor> {

	public FloorRepository() {
		super(Floor.class);
	}

}
