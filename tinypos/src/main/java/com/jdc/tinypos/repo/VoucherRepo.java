package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.Voucher;

@Repository
public class VoucherRepo extends AbstractRepository<Voucher>{

	public VoucherRepo() {
		super(Voucher.class);
	}

}
