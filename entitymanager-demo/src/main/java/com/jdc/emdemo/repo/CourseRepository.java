package com.jdc.emdemo.repo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.jdc.emdemo.entity.Course;

public class CourseRepository {

	private EntityManager em;

	public CourseRepository(EntityManager em) {
		super();
		this.em = em;
	}

	public void create(Course c) {
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();
	}

	public void update(Course c) {
		em.getTransaction().begin();
		em.merge(c);
		em.getTransaction().commit();
	}

	public void delete(int id) {
		em.getTransaction().begin();
		Query q = em.createNamedQuery("Course.deleteById");
		q.setParameter("id", id);
		q.executeUpdate();
		em.getTransaction().commit();
	}

	public Course findById(int id) {
		return em.find(Course.class, id);
	}

	public List<Course> findAll() {
		return em.createNamedQuery("Course.findAll", Course.class).getResultList();
	}

	public List<Course> findByNameLike(String name) {
		return em.createNamedQuery("Course.findAll", Course.class)
				.setParameter("name", name.concat("%"))
				.getResultList();
	}

}
