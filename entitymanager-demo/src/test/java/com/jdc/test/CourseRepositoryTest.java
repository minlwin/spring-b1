package com.jdc.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.jdc.emdemo.entity.Course;
import com.jdc.emdemo.repo.CourseRepository;

class CourseRepositoryTest {

	static EntityManagerFactory emf;
	EntityManager em;
	CourseRepository repo;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("entitymanager-demo");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@BeforeEach
	void setUp() throws Exception {
		em = emf.createEntityManager();
		repo = new CourseRepository(em);
	}
	
	@AfterEach
	void finish() {
		em.close();
	}

	@Test
	void test() {
		List<Course> list = repo.findAll();
		assertEquals(6, list.size());
	}

}
