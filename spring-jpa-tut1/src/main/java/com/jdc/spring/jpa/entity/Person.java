package com.jdc.spring.jpa.entity;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.TABLE;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: Person
 *
 */
@Entity
@Table(name = "PERSON_TBL")
@SecondaryTables({
	@SecondaryTable(name = "PROFILE"),
	@SecondaryTable(name = "LOGIN_TBL")
})
public class Person implements Serializable {

	public enum MaritalStatus {
		Marrid, Single
	}

	@Id
	@GeneratedValue(strategy = TABLE)
	@Column(name = "pserson_code")
	private int id;
	private String name;
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	private Date dob;
	private String nrc;
	
	@Column(table = "LOGIN_TBL", name = "login_id")
	private String loginId;
	@Column(table = "LOGIN_TBL")
	private String password;

	@Enumerated(EnumType.STRING)
	@Column(name = "marital_status", table = "PROFILE")
	private MaritalStatus maritalStatus;

	@Lob
	@Basic(fetch = LAZY)
	@Column(table = "PROFILE")
	private String description;

	@Transient
	private boolean selected;
	
	private SecurityInfo security;

	public Person() {
		super();
		security = new SecurityInfo();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getNrc() {
		return nrc;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

}
