package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.SellPrice;

@Repository
public class SellPriceRepo extends AbstractRepository<SellPrice>{

	public SellPriceRepo() {
		super(SellPrice.class);
	}

}
