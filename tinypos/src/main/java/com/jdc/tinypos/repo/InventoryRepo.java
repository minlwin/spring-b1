package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.Inventory;

@Repository
public class InventoryRepo extends AbstractRepository<Inventory>{

	public InventoryRepo() {
		super(Inventory.class);
	}

}
