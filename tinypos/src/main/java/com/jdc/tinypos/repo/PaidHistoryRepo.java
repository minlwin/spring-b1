package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.PaidHistory;

@Repository
public class PaidHistoryRepo extends AbstractRepository<PaidHistory>{

	public PaidHistoryRepo() {
		super(PaidHistory.class);
	}

}
