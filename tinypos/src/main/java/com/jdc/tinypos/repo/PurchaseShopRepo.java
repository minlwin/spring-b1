package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.PurchaseShop;

@Repository
public class PurchaseShopRepo extends AbstractRepository<PurchaseShop>{

	public PurchaseShopRepo(Class<PurchaseShop> type) {
		super(PurchaseShop.class);
	}

}
