package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.Employee;

@Repository
public class EmployeeRepo extends AbstractRepository<Employee>{

	public EmployeeRepo() {
		super(Employee.class);
	}
}
