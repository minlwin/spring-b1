<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Item</title>
</head>
<body>

	<c:choose>
		<c:when test="${item.id eq 0}">
			<h1>Add New Item</h1>
		</c:when>
		
		<c:otherwise>
			<h1>Edit Item</h1>
		</c:otherwise>
	</c:choose>
	
	<c:url value="/items" var="save" />

	<s:form action="${save}" method="post" modelAttribute="item">
		<s:hidden path="id"/>
		<table>

			<tr>
				<td>Category</td>
				<td>
					<s:select path="category" >
						<s:options items="${categories}"  />
					</s:select>
					<s:errors path="category" />
				</td>
			</tr>
			
			<tr>
				<td>Name</td>
				<td>
					<s:input path="name"/>
					<s:errors path="name" />
				</td>
			</tr>

			<tr>
				<td>Price</td>
				<td>
					<s:input path="price"/>
					<s:errors path="price" />
				</td>
			</tr>

			<tr>
				<td></td>
				<td>
					<s:button>Save</s:button>
				</td>
			</tr>

		</table>		
		
	</s:form>
	
</body>
</html>