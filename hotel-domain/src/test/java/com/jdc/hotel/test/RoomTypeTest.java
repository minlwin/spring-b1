package com.jdc.hotel.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jdc.hotel.entity.RoomType;
import com.jdc.hotel.repo.RoomTypeRepository;

class RoomTypeTest {
	
	static ConfigurableApplicationContext ctx;
	
	RoomTypeRepository repo;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new ClassPathXmlApplicationContext("application.xml");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}
	
	@BeforeEach
	void setUp() {
		repo = ctx.getBean(RoomTypeRepository.class);
	}

	@Test
	@DisplayName("Can Insert Test")
	void test1() {
		
		RoomType type = new RoomType();
		type.setType("Single Room");
		type.addFacility("Bed", "1");
		type.addFacility("Shower Room", "Small Room");
		type.setPrice(20000);
		
		repo.create(type);
		
		assertEquals(1, type.getId().intValue());
	}
	
	@Test
	@DisplayName("Can Find By Id Test")
	void test2() {
		
		RoomType type = repo.findById(1);
		assertNotNull(type);
		
		assertEquals("Single Room", type.getType());
		assertEquals(20000, type.getPrice());
		
		assertNotNull(type.getFacilities());
		assertEquals(2, type.getFacilities().size());
		
		assertEquals("1", type.getFacilities().get("Bed"));
		assertEquals("Small Room", type.getFacilities().get("Shower Room"));
		
		assertNull(repo.findById(2));
	}
	
	@Test
	@DisplayName("Update Test")
	void test3() {
		
		RoomType type = repo.findById(1);
		
		type.addFacility("TV", "24 Inches TV");
		
		repo.update(type);
		
		RoomType result = repo.findById(1);
		assertEquals("24 Inches TV", result.getFacilities().get("TV"));
	}

	@Test
	@DisplayName("Delete Test")
	void test4() {
		RoomType type = repo.findById(1);
		repo.delete(type);
		
		assertNull(repo.findById(1));
	}
}
