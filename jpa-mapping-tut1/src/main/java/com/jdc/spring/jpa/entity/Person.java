package com.jdc.spring.jpa.entity;

import static javax.persistence.DiscriminatorType.INTEGER;
import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.InheritanceType.JOINED;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;

/**
 * Entity implementation class for Entity: Person
 *
 */
@Entity

@DiscriminatorColumn(discriminatorType = INTEGER, length = 1)
@DiscriminatorValue("1")
@Inheritance(strategy = JOINED)
public class Person implements Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	private String name;
	private static final long serialVersionUID = 1L;

	@ElementCollection
	@CollectionTable(name = "PERSON_INTEREST")
	private Set<String> interests;

	@ElementCollection
	@CollectionTable(name = "PERSON_PHONE")
	@MapKeyColumn(name = "type")
	private Map<String, String> phones;

	@ManyToOne
	@JoinTable(name = "PERSON_ADDRESS")
	private Address address;

	public Person() {
		super();
		interests = new LinkedHashSet<>();
		phones = new LinkedHashMap<>();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getInterests() {
		return interests;
	}

	public void setInterests(Set<String> interests) {
		this.interests = interests;
	}

	public Map<String, String> getPhones() {
		return phones;
	}

	public void setPhones(Map<String, String> phones) {
		this.phones = phones;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
