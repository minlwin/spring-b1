package com.jdc.hotel.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.jdc.hotel.repo.JpaEntity;

@Entity
public class RoomReservation implements JpaEntity {

	private static final long serialVersionUID = 1L;

	public RoomReservation() {
	}

	@EmbeddedId
	private RoomReservationPK id;

	@ManyToOne
	@JoinColumn(name = "reservationId", insertable = false, updatable = false)
	private Reservation reservation;

	public RoomReservationPK getId() {
		return id;
	}

	public void setId(RoomReservationPK id) {
		this.id = id;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

}