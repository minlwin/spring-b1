#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ${package}.entity.Person;

@Repository
public class PersonRepo {
	
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void create(Person p) {
		em.persist(p);
	}

}
