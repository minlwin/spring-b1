#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import ${package}.entity.Person;

public interface PersonRepo extends JpaRepository<Person, Integer>{

}
