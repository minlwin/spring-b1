package com.jdc.hotel.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.jdc.hotel.repo.JpaEntity;

@Entity
public class Customer implements JpaEntity {

	private static final long serialVersionUID = 1L;

	public Customer() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long id;

	private String name;

	private String phone;

	private String email;

	private String passportOrNrc;

	private String nationality;

	private Gender gender;

	public enum Gender {
		Male, Female
	}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassportOrNrc() {
		return passportOrNrc;
	}

	public void setPassportOrNrc(String passportOrNrc) {
		this.passportOrNrc = passportOrNrc;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

}