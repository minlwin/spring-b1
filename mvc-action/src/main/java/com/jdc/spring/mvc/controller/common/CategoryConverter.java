package com.jdc.spring.mvc.controller.common;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import com.jdc.spring.mvc.entity.Category;
import com.jdc.spring.mvc.service.CategoryService;

@Component
public class CategoryConverter implements Formatter<Category> {

	@Autowired
	private CategoryService service;

	@Override
	public String print(Category object, Locale locale) {
		
		if(null != object) {
			return String.valueOf(object.getId());
		}
		
		return null;
	}

	@Override
	public Category parse(String source, Locale locale) throws ParseException {
		if(null != source) {
			int id = Integer.parseInt(source);
			return service.findById(id).orElse(null);
		}
		return null;
	}
}
