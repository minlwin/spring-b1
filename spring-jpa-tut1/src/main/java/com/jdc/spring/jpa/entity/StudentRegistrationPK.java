package com.jdc.spring.jpa.entity;

import java.io.Serializable;
import java.util.Date;

public class StudentRegistrationPK implements Serializable {

	private static final long serialVersionUID = 1L;
	private int personId;
	private Date registDate;

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public Date getRegistDate() {
		return registDate;
	}

	public void setRegistDate(Date registDate) {
		this.registDate = registDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + personId;
		result = prime * result + ((registDate == null) ? 0 : registDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentRegistrationPK other = (StudentRegistrationPK) obj;
		if (personId != other.personId)
			return false;
		if (registDate == null) {
			if (other.registDate != null)
				return false;
		} else if (!registDate.equals(other.registDate))
			return false;
		return true;
	}

}
