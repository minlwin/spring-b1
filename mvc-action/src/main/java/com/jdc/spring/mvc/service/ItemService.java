package com.jdc.spring.mvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdc.spring.mvc.entity.Item;
import com.jdc.spring.mvc.repo.ItemRepo;

@Service
public class ItemService {
	
	@Autowired
	private ItemRepo repo;

	public List<Item> getAll() {
		return repo.findAll();
	}

	public Item findById(int id) {
		return repo.findById(id).orElse(null);
	}

	@Transactional
	public void save(Item item) {
		repo.save(item);
	}

}
