<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Category</title>
</head>
<body>
	
	<h1>
		<c:choose>
			<c:when test="${category.id eq 0}">Add Category</c:when>
			<c:otherwise>Edit Category</c:otherwise>
		</c:choose>
	</h1>
	
	<c:url value="/categories" var="action"></c:url>
	<s:form action="${action}" method="post" modelAttribute="category">
		<s:hidden path="id"/>
		<s:label path="name">Name</s:label>
		<s:input path="name"/>
		<s:errors path="name"></s:errors>
		
		<s:button>Save</s:button>
	
	</s:form>
</body>
</html>