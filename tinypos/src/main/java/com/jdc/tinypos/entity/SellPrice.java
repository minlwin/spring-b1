package com.jdc.tinypos.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class SellPrice implements Serializable {

	private static final long serialVersionUID = 1L;

	public SellPrice() {
	}

	private int price;

	private SecurityInfo security;

	@ManyToOne
	@JoinColumn(name = "productId", insertable = false, updatable = false)
	private Product product;

	@OneToOne
	private SellPrice lastPrice;

	@EmbeddedId
	private ProductDatePK id;

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public SellPrice getLastPrice() {
		return lastPrice;
	}

	public void setLastPrice(SellPrice lastPrice) {
		this.lastPrice = lastPrice;
	}

	public ProductDatePK getId() {
		return id;
	}

	public void setId(ProductDatePK id) {
		this.id = id;
	}

}