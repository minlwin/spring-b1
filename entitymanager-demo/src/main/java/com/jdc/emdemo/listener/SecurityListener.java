package com.jdc.emdemo.listener;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.jdc.emdemo.entity.SecurityInfo;

public class SecurityListener {

	@PrePersist
	public void prePersist(Object object) {

		if (object instanceof SecureEntity) {
			SecureEntity entity = (SecureEntity) object;
			SecurityInfo sec = new SecurityInfo();
			sec.setCreation(LocalDateTime.now());
			sec.setDelFlag(false);
			entity.setSecurity(sec);
		}
	}

	@PreUpdate
	public void preUpdate(Object object) {

		if (object instanceof SecureEntity) {
			SecureEntity entity = (SecureEntity) object;
			entity.getSecurity().setModification(LocalDateTime.now());
		}
	}

}
