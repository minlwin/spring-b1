package com.jdc.spring.mvc.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jdc.spring.mvc.entity.Item;

public interface ItemRepo extends JpaRepository<Item, Integer>{

}
