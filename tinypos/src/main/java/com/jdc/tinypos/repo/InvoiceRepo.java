package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.Inventory;

@Repository
public class InvoiceRepo extends AbstractRepository<Inventory>{

	public InvoiceRepo() {
		super(Inventory.class);
	}

}
