package com.jdc.spring.jpa.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Entity implementation class for Entity: Student
 *
 */
@Entity
@DiscriminatorValue("2")
public class Student extends Person implements Serializable {

	private String school;
	private String grade;
	private static final long serialVersionUID = 1L;

	public Student() {
		super();
	}

	public String getSchool() {
		return this.school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

}
