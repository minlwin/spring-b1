package com.jdc.spring.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jdc.spring.jpa.entity.Person;

public interface PersonRepo extends JpaRepository<Person, Integer>{

}
