package com.jdc.hotel.repo;

import java.io.Serializable;

public interface JpaEntity extends Serializable {
	Object getId();
}
