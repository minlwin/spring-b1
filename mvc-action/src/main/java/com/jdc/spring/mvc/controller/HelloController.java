package com.jdc.spring.mvc.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("hello")
public class HelloController {
	
	@GetMapping
	public void hello(Map<String, String> model) {
		model.put("message", "Message from Controller Method");
	}
	
	@ModelAttribute(name = "message")
	public String message() {
		return "Message from Model Attribute";
	}
}
