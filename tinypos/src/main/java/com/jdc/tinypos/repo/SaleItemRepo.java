package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.SaleItem;

@Repository
public class SaleItemRepo extends AbstractRepository<SaleItem>{

	public SaleItemRepo() {
		super(SaleItem.class);
	}

}
