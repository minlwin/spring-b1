package com.jdc.spring.mvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jdc.spring.mvc.controller.common.CategoryConverter;
import com.jdc.spring.mvc.entity.Category;
import com.jdc.spring.mvc.entity.Item;
import com.jdc.spring.mvc.service.CategoryService;
import com.jdc.spring.mvc.service.ItemService;

@Controller
@RequestMapping("items")
public class ItemController {
	
	@Autowired
	private ItemService service;
	
	@Autowired
	private CategoryService catService;
	
	@Autowired
	private CategoryConverter converter;
	
	@InitBinder
	public void init(WebDataBinder binder) {
		binder.addCustomFormatter(converter);
	}
	
	
	@GetMapping
	public String getAll(ModelMap model) {
		List<Item> list = service.getAll();
		model.put("list", list);
		return "item-list";
	}
	
	@GetMapping("add")
	public String addItem(ModelMap model) {
		model.put("item", new Item());
		return "item-edit";
	}
	
	@GetMapping("edit")
	public String editItem(@RequestParam int id, ModelMap model) {
		Item item = service.findById(id);
		model.put("item", item);
		return "item-edit";
	}
	
	@PostMapping
	public String save(@ModelAttribute(name= "item") @Valid Item item, BindingResult result) {
		
		if(result.hasErrors()) {
			return "/item-edit";
		}
		
		service.save(item);
		
		return "redirect:/items";
	}
	
	@ModelAttribute("categories")
	public List<Category> categories() {
		return catService.getAll();
	}

}
