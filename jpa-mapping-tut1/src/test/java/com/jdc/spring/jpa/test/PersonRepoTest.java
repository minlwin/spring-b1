package com.jdc.spring.jpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jdc.spring.jpa.entity.Person;
import com.jdc.spring.jpa.repo.PersonRepo;

class PersonRepoTest {
	
	static ConfigurableApplicationContext ctx;
	PersonRepo repo;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new ClassPathXmlApplicationContext("application.xml");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}

	@BeforeEach
	void setUp() throws Exception {
		repo = ctx.getBean(PersonRepo.class);
	}

	@Test
	void test() {
		Person p = new Person();
		p.setName("Aung Aung");
		repo.create(p);
		
		assertEquals(1, p.getId());
	}

}
