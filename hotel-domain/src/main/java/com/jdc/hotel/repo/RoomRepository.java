package com.jdc.hotel.repo;

import org.springframework.stereotype.Repository;

import com.jdc.hotel.entity.Room;

@Repository
public class RoomRepository extends BaseRepository<Room>{

	public RoomRepository() {
		super(Room.class);
	}
}
