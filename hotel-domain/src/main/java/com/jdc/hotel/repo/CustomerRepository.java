package com.jdc.hotel.repo;

import org.springframework.stereotype.Repository;

import com.jdc.hotel.entity.Customer;

@Repository
public class CustomerRepository extends BaseRepository<Customer> {
	
	public CustomerRepository() {
		super(Customer.class);
	}
}
