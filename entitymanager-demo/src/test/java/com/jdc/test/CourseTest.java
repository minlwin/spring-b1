package com.jdc.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.jdc.emdemo.entity.Branch;
import com.jdc.emdemo.entity.ClassRoom;
import com.jdc.emdemo.entity.Course;

class CourseTest {

	static EntityManagerFactory emf;

	EntityManager em;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("entitymanager-demo");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@BeforeEach
	void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@Test
	void test1() {

		Course c = new Course();
		c.setName("Java SE");
		c.setPrice(180000);

		em.getTransaction().begin();

		em.persist(c);

		em.getTransaction().commit();

		em.close();

		// Create a new em
		em = emf.createEntityManager();

		Course c1 = em.getReference(Course.class, 1);

		assertEquals(c.getName(), c1.getName());
		assertEquals(c.getPrice(), c1.getPrice());
	}

	@Test
	void test2() {

		Branch b = new Branch();
		b.setName("Yangon");

		ClassRoom r1 = new ClassRoom();
		r1.setRoomNum("Room1");

		ClassRoom r2 = new ClassRoom();
		r2.setRoomNum("Room2");

		b.addRoom(r1);
		b.addRoom(r2);

		em.getTransaction().begin();

//		em.persist(r1);
//		em.persist(r2);
		em.persist(b);

		em.getTransaction().commit();
	}

}
