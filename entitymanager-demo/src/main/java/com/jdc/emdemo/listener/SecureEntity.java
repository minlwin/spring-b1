package com.jdc.emdemo.listener;

import java.io.Serializable;

import com.jdc.emdemo.entity.SecurityInfo;

public interface SecureEntity extends Serializable {

	SecurityInfo getSecurity();

	void setSecurity(SecurityInfo security);
}
