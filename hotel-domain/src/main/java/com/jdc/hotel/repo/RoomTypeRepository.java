package com.jdc.hotel.repo;

import org.springframework.stereotype.Repository;

import com.jdc.hotel.entity.RoomType;

@Repository
public class RoomTypeRepository extends BaseRepository<RoomType>{

	public RoomTypeRepository() {
		super(RoomType.class);
	}
	
}
