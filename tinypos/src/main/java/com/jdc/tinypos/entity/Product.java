package com.jdc.tinypos.entity;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
	@NamedQuery(name = "Product.findByCategory", query = "select p from Product p where p.category = :category"),
	@NamedQuery(name = "Product.findByDateAndPrice", query = "select p from Product p join p.prices pr "
			+ "where pr.price <= :price and pr.id.productId = (select max(sp.id.productId) from SellPrice sp where sp.id.refDate <= :date) "
			+ "and pr.id.refDate = (select max(sp.id.refDate) from SellPrice sp where sp.id.refDate <= :date)"),
})
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	public Product() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;

	private String name;

	private String image;

	private SecurityInfo security;

	@ManyToOne
	private Category category;
	
	@OneToMany(mappedBy = "product", cascade = { PERSIST, MERGE }, orphanRemoval = true)
	private List<SellPrice> prices;
	

	public List<SellPrice> getPrices() {
		return prices;
	}

	public void setPrices(List<SellPrice> prices) {
		this.prices = prices;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	

}