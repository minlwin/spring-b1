#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ${package}.SpringJpaApplication;
import ${package}.entity.Person;
import ${package}.repo.PersonRepo;

class SpringJpaApplicationTest {
	
	private static ConfigurableApplicationContext ctx;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new AnnotationConfigApplicationContext(SpringJpaApplication.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}
	

	@Test
	void test() {
		PersonRepo repo = ctx.getBean(PersonRepo.class);
		Person p = new Person();
		p.setName("Jhon Snow");
		repo.save(p);
		
		assertEquals(1, p.getId());
	}

}
