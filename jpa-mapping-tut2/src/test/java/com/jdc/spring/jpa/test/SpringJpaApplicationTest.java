package com.jdc.spring.jpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.jdc.spring.jpa.entity.Address;
import com.jdc.spring.jpa.entity.Person;

class SpringJpaApplicationTest {

	static EntityManagerFactory emf;

	EntityManager em;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("jpa-mapping-tut2");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@BeforeEach
	void before() {
		em = emf.createEntityManager();
	}

	@AfterEach
	void after() {
	}

	@Test
	void test1() {

		Person p = new Person();
		p.setName("Jhon Snow");

		Address a = new Address();
		a.setName("Yangon");

		p.setAddress(a);

		em.getTransaction().begin();

		em.persist(p);

		em.getTransaction().commit();

		assertEquals(1, p.getId());

		em.close();
	}

	@Test
	void test2() {

		Person p = em.getReference(Person.class, 1);

		em.getTransaction().begin();

		em.remove(p);

		em.getTransaction().commit();

	}

}
