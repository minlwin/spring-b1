package com.jdc.tinypos.repo;

import org.springframework.stereotype.Repository;

import com.jdc.tinypos.entity.Product;

@Repository
public class ProductRepo extends AbstractRepository<Product>{

	public ProductRepo() {
		super(Product.class);
	}
}
