package com.jdc.tinypos.test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

class PersonRepoTest {
	
	static ConfigurableApplicationContext ctx;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new ClassPathXmlApplicationContext("application.xml");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}

	@Test
	void test() {
	}

}
