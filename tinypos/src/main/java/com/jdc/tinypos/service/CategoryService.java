package com.jdc.tinypos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdc.tinypos.entity.Category;
import com.jdc.tinypos.repo.CategoryRepo;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepo repo;
	
	public List<Category> getAll() {
		return repo.selectNamedQuery("Category.getAll", null);
	}
	
	@Transactional
	public void createt(Category c) {
		repo.create(c);
	}
}
