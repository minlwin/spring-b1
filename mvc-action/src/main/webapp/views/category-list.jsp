<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Category List</title>
</head>
<body>

	<h1>Category List</h1>
	
	<c:url value="/categories/edit" var="add" />
	
	<a href="${add}">Add New Category</a>
	
	<table>
		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Delete</td>
			<td>Edit</td>
		</tr>
		
		<c:forEach items="${list}" var="c">
			<tr>
				<td>${c.id}</td>
				<td>${c.name}</td>
				<td>Delete</td>
				<td>
					<c:url var="edit" value="/categories/edit/${c.id}" />
					<a href="${edit}">Edit</a>
				</td>
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>