package com.jdc.hotel.repo;

import org.springframework.stereotype.Repository;

import com.jdc.hotel.entity.RoomReservation;

@Repository
public class RoomReservationRepository extends BaseRepository<RoomReservation>{

	public RoomReservationRepository() {
		super(RoomReservation.class);
	}
}
